;;;; DNA and RNA ;;;;

;;; double helix ;;;
(defun complement-base (base)
  "return the complement of BASE"
  (cond ((equal base 'a) 't)
        ((equal base 't) 'a)
        ((equal base 'c) 'g)
        ((equal base 'g) 'c)
        ((equal base 'u) 'a)
        (t (list base 'not 'known))))

(defun complement-strand (dna)
  "return complement-strand of single DNA"
  (mapcar #'complement-base dna))

(defun make-double (dna)
  "return double dna of single DNA"
  (mapcar #'(lambda (base) (list base (complement-base base))) dna))

(defun count-bases (strand)
  "return number of each base in DNA"
  (let ((a-count 0)
	(t-count 0)
	(g-count 0)
	(c-count 0))
    (labels ((count-single-strand (base)
	       (cond ((equal base 'a) (incf a-count))
	             ((equal base 't) (incf t-count))
	             ((equal base 'g) (incf g-count))
	             ((equal base 'c) (incf c-count)))))
      (dolist (e strand)
	(cond ((atom e) (count-single-strand e))
	      (t (count-single-strand (first e))
	         (count-single-strand (second e)))))
      (list (list 'a a-count)
	    (list 't t-count)
	    (list 'g g-count)
	    (list 'c c-count)))))

(defun prefixp (s1 s2)
  "return T if S1 is prefix of S2"
  (do ((a s1 (rest a))
       (b s2 (rest b)))
      ((null a) t)
    (if (not (equal (first a) (first b)))
	(return nil))))

(defun appearsp (s1 s2)
  "return T if S1 appears in S2"
  (do ((b s2 (rest b)))
      ((null b) nil)
    (if (prefixp s1 b)
	(return t))))

(defun coverp (s1 s2)
  "return T if s2 is s1 repeated several times"
  (do ((b s2 (nthcdr (length s1) b)))
      ((null b) t)
    (if (not (prefixp s1 b))
	(return nil))))

(defun prefix (n dna)
  "return the leftmost N elements of DNA"
  (do ((i 0 (+ i 1))
       (a dna (rest a))
       (result nil (cons (first a) result)))
      ((equal i n) (reverse result))))

(defun kernel (dna)
  "return the shortest list that make DNA"
  (do ((i 1 (+ i 1)))
      ((coverp (prefix i dna) dna)
       (prefix i dna))))
