;;;; throw dice game of craps ;;;;

(defun throw-die ()
  "return a random number from 1 to 6"
  (+ 1 (random 6)))

(defun throw-dice ()
  "throw 2 dice and return a list of 2 numbers"
  (list (throw-die) (throw-die)))

(defun snake-eyes-p (pair)
  "return T for (1 1), NIL otherwise"
  (if (equal '(1 1) pair)
      T
      NIL))

(defun boxcars-p (pair)
  "return T for (6 6), NIL otherwise"
  (if (equal '(6 6) pair)
      T
      NIL))

(defun sum-dice (pair)
  "return the sum of two dice"
  (+ (first pair) (second pair)))

(defun instant-win-p (throw)
  "return T if throw is 7 or 11,
NIL otherwise"
  (if (or (= 7 (sum-dice throw))
          (= 11 (sum-dice throw)))
      T
      NIL))

(defun instant-loss-p (throw)
  "return T if throw is 7 or 11,
NIL otherwise"
  (if (or (= 2 (sum-dice throw))
          (= 3 (sum-dice throw))
          (= 12 (sum-dice throw)))
      T
      NIL))

(defun say-throw (throw)
  "return SNAKE-EYES, BOXCARS or sum of throw"
  (cond ((snake-eyes-p throw) 'snake-eyes)
        ((boxcars-p throw) 'boxcars)
        (T (sum-dice throw))))

(defun craps ()
  "return
(throw first-die and second-die -- snake-eyes/boxcars -- you win/lose)
or
(throw first-die and second-die -- sum of first-die and second-die)
accordingly"
  (let* ((throw (throw-dice))
         (first-die (first throw))
         (second-die (second throw))
         (points (sum-dice throw)))
    (cond ((instant-loss-p throw) (list 'throw first-die 'and second-die '-- (say-throw throw) '-- 'you 'lose))
	  ((instant-win-p throw) (list 'throw first-die 'and second-die '-- (say-throw throw) '-- 'you 'win))
	  (T (list 'throw first-die 'and second-die '-- (say-throw throw) '-- 'your 'point 'is points)))))

(defun try-for-point (point)
  "try to throw the point"
  (let* ((throw (throw-dice))
         (first-die (first throw))
         (second-die (second throw))
         (points (sum-dice throw)))
    (cond ((= point points) (list 'throw first-die 'and second-die '-- points '-- 'you 'win))
	  (T (list 'throw first-die 'and second-die '-- points '-- 'try 'again)))))
