;;;; blocks world ;;;;

;;; blocks as triples of form (block attribute value) ;;;
(defvar database
      '((b1 shape brick)
        (b1 color green)
        (b1 size small)
        (b1 supported-by b2)
        (b1 supported-by b3)
        (b2 shape brick)
        (b2 color red)
        (b2 size small)
        (b2 supports b1)
        (b2 left-of b3)
        (b3 shape brick)
        (b3 color red)
        (b3 size small)
        (b3 supports b1)
        (b3 right-of b2)
        (b4 shape pyramid)
        (b4 color blue)
        (b4 size large)
        (b4 supported-by b5)
        (b5 shape cube)
        (b5 color green)
        (b5 size large)
        (b5 supports b4)
        (b6 shape brick)
        (b6 color purple)
        (b6 size large)))

(defun match-element (symbol1 symbol2)
  "return T if 2 symbols are equal, or symbol2 is ?"
  (or (equal symbol1 symbol2) (equal symbol2 '?)))

(defun match-triple (assertion pattern)
  "return T if assertion matches pattern, both inputs
are 3-element list."
  (every #'match-element assertion pattern))

(defun fetch (pattern)
  "return all element in DATABASE that matches PATTERN"
  (remove-if-not #'(lambda (s) (match-triple s pattern)) database))

(defun ask-color (block)
  "return pattern to ask the color of BLOCK"
  (list block 'color '?))

(defun supporters (block)
  "return all supporters of BLOCK in DATABASE"
  (mapcar #'first (fetch `(? supports ,block))))

(defun cubes ()
  "return all cubes of BLOCK in DATABASE"
  (mapcar #'first (fetch '(? ? cube))))

(defun supp-cube (block)
  "return T is BLOCK is supported by a cube"
  (not (null (intersection (supporters block) (cubes)))))

(defun desc1 (block)
  "return all assertions of BLOCK in DATABASE"
  (fetch `(,block ? ?)))

(defun desc2 (block)
  "stripe desc1 to remove BLOCK"
  (mapcar #'rest (desc1 block)))

(defun description (block)
  "return description of BLOCK"
  (reduce #'append (desc2 block)))
