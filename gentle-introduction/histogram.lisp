;;;; hisstogram drawing ;;;;

;;; use array and point to draw histogram ;;;
(defvar *hist-array* nil)    ; array of counts
(defvar *total-points* nil)  ; number of points so far

(defun new-histogram (no-of-bins)
  "make a NO-OF-BINS array"
  (setf *hist-array* (make-array no-of-bins :initial-element 0))
  (setf *total-points* 0))

(defun record-value (bin-index)
  "increase the value of array if BIN-INDEX is within range of array"
  (cond ((< -1 bin-index (length *hist-array*))
         (incf (aref *hist-array* bin-index))
         (incf *total-points*))
        (t (format t "~&bin-index ~S is out of range~%" bin-index)))
  (force-output))

(defun print-hist-line (bin-index)
  "print the BIN-INDEX element of the array"
  (let ((bin-value (aref *hist-array* bin-index)))
    (format t "~&~2D [~3D] " bin-index bin-value)
    (dotimes (i bin-value)
      (format t "*"))))

(defun print-histogram ()
  "print all array"
  (dotimes (i (length *hist-array*))
    (print-hist-line i)
    (format t "~%"))
  (and *total-points* (format t "~&~8D total~%" *total-points*)))
