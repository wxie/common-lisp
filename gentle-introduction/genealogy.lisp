;;;; genealogy ;;;;

;;; family databas (name father mother) ;;;
(defvar family
      '((colin nil nil)
        (deirdre nil nil)
        (arthur nil nil)
        (kate nil nil)
        (frank nil nil)
        (linda nil nil)
        (suzanne colin deirdre)
        (bruce arthur kate)
        (charles arthur kate)
        (david arthur kate)
        (ellen arthur kate)
        (george frank linda)
        (hillary frank linda)
        (andre nil nil)
        (tamara bruce suzanne)
        (vincent bruce suzanne)
        (wanda nil nil)
        (ivan george ellen)
        (julie george ellen)
        (marie george ellen)
        (nigel andre hillary)
        (frederick nil tamara)
        (zelda vincent wanda)
        (joshua ivan wanda)
        (quentin nil nil)
        (robert quentin julie)
        (olivia nigel marie)
        (peter nigel marie)
        (erica nil nil)
        (yvette robert zelda)
        (diane peter erica)))

(defun father (person)
  "return FATHER of PERSON"
  (second (assoc person family)))

(defun mother (person)
  "return FATHER of PERSON"
  (third (assoc person family)))

(defun parents (person)
  "return FATHER of PERSON"
  (remove-if #'null (rest (assoc person family))))

(defun children (person)
  "return children of PERSON"
  (remove-if #'null (mapcar #'(lambda (p)
                                (if (or (equal person (second p))
                                        (equal person (third p)))
                                    (first p)))
                            family)))

(defun siblings (person)
  "return PERSON's siblings"
  (set-difference (union (children (mother person))
                         (children (father person)))
                  (list person)))

(defun mapunion (fn ls)
  "take a function FN and a list LS
apply FN to every element of LS
return the union of all result"
  (reduce #'union (mapcar fn ls)))

(defun grandparents (person)
  "return grandparents of PERSON"
 (mapunion #'parents (parents person)))

(defun cousins (person)
  "return PERSON's cousins"
  (mapunion #'children (mapunion #'siblings (parents person))))

(defun descended-from (p1 p2)
  "return T if P1 descended from P2"
  (cond ((null p1) nil)
        ((member p2 (parents p1)) T)
        (t (or (descended-from (father p1) p2)
               (descended-from (mother p1) p2)))))

(defun ancestors (person)
  "return PERSON's ancestors"
  (if (null person)
      nil
      (union (parents person)
             (union (ancestors (father person))
                    (ancestors (mother person))))))

(defun generation-gap-recursive (depth p1 p2)
 (cond ((null p1) nil)
       ((equal p1 p2) depth)
       (t (or (generation-gap-recursive (+ 1 depth) (father p1) p2)
              (generation-gap-recursive (+ 1 depth) (mother p1) p2)))))

(defun generation-gap (p1 p2)
  "return number of generations between P1 and P2"
  (if (not (descended-from p1 p2))
      nil
      (generation-gap-recursive 0 p1 p2)))
