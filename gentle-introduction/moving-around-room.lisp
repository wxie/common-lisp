;;;; move around a room ;;;;

;;; map of the house ;;;
(setf ROOMS
      ;; A house has elements with directions to elements.
      '((living-room
	 (north front-stairs)
	 (south dining-room)
	 (east kitchen))
	(kitchen
	 (south pantry)
	 (west living-room))
	(pantry
	 (north kitchen)
	 (west dining-room))
	(dining-room
	 (north living-room)
	 (west downstairs-bedroom)
	 (east pantry))
	(downstairs-bedroom
	 (east dining-room)
	 (north back-stairs))
	(back-stairs
	 (south downstairs-bedroom)
	 (north library))
	(library
	 (south back-stairs)
	 (east upstairs-bedroom))
	(upstairs-bedroom
	 (west library)
	 (south front-stairs))
	(front-stairs
	 (north upstairs-bedroom)
	 (south living-room))))

(setf LOC
      ;; current location of robbie
      nil)

(defun choices (room)
  "return directions of ROOM"
  (cdr (assoc room ROOMS)))

(defun look (direction room)
  "return the element of DIRECTION in ROOM"
  (cdr (assoc direction (choices room))))

(defun set-robbie-location (place)
  "move Robbie to PLACE by setting LOC"
  (setf LOC place))

(defun how-many-choices ()
  "return choices of directions of LOC"
  (length (choices LOC)))

(defun upstairsp (place)
  "return T is PLACE is upstairs, otherwise NIL"
  (or (equal 'upstairs-bedroom place)
      (equal 'library place)))

(defun onstairsp (place)
  "return T is PLACE is onstairs, otherwise NIL"
  (or (equal 'back-stairs place)
      (equal 'front-stairs place)))

(defun where ()
  "return where robbie is"
  (cond
    ((upstairsp LOC) (list 'Robbie 'is 'upstairs 'in 'the LOC))
    ((onstairsp LOC) (list 'Robbie 'is 'on 'the LOC))
    (T (list 'Robbie 'is 'downstairs 'in 'the LOC))))

(defun move (direction)
  "move Robbie in DIRECTION, say new location"
  (cond ((not (null (look direction LOC)))
         (set-robbie-location (car (look direction LOC)))
         (where))
        (T
         (list 'Ouch! 'Robbie 'hits 'a 'wall))))
