;;;; playing cards ;;;;

;;; a hand of cards with rank and suit ;;;
(setf MY-HAND
      '((3 hearts)
	(5 clubs)
	(2 diamonds)
	(4 diamonds)
	(ace spades)))

(defvar COLORS
  '((clubs black)(diamonds red)(hearts red)(spades black)))

(defvar ALL-RANKS
  '(2 3 4 5 6 7 8 9 10 jack queen king ace))

(defun rank (card)
  "return the RANK of CARD"
  (first card))

(defun suit (card)
  "return the SUIT of CARD"
  (second card))

(defun count-suit (suit hand)
  "return number of SUIT cards in HAND"
  (length (remove-if-not #'(lambda (card)
			     (equal suit (suit card)))
			 hand)))

(defun color-of (card)
  "return the color of card according to COLORS"
  (second (assoc (suit card) COLORS)))

(defun first-red (hand)
  "return the first red card in HAND"
  (first (remove-if-not #'(lambda (card)
                            (equal 'red (color-of card)))
                        hand)))

(defun black-card (hand)
  "return all black cards in HAND"
  (remove-if-not #'(lambda (card)
                     (equal 'black (color-of card)))
                 hand))

(defun what-ranks (suit hand)
  "return all RANKs of SUIT in HAND"
  (mapcar #'first
       (remove-if-not #'(lambda (card)
                          (equal suit (suit card)))
                      hand)))

(defun high-rank-p (card1 card2)
  "return T if rank of card1 is higher than card2"
  (null
   (member (rank card2) (member (rank card1) all-ranks))))

(defun high-card (hand)
  "return the highest ranked card in HAND"
  (assoc (find-if #'(lambda (r)
		      (assoc r hand))
		  (reverse all-ranks))
	 hand))
