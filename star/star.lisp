;;;; This is a sample lisp program to display stars on the screen.
;;;; It uses cl-sdl2 from quicklisp: (quicklisp:quickload 'sdl2)

(require :sdl2)

(defstruct star
  angle radius speed brightness alpha)

(defun random-from-range (start end)
  (+ start (random (+ 1 (- end start)))))

(defun create-star ()
  (let ((angle (* (random 1.0) 2 pi))
        (radius (sqrt (random 1.0)))) ;; Ensures even distribution
    (make-star
     :angle angle
     :radius radius
     :speed (+ 0.002 (random 0.003))
     :brightness (random-from-range 100 255)
     :alpha (random-from-range 200 255))))

(defun create-stars (n)
  (let ((stars (make-array n)))
    (dotimes (i n)
      (setf (aref stars i) (create-star)))
    stars))

(defun render-starfield (r stars width height)
  (sdl2:set-render-draw-color r 0 0 0 255)
  (sdl2:render-clear r)
  (dotimes (i (length stars))
    (let* ((s (aref stars i))
           (value (star-brightness s))
           (alpha (star-alpha s))
           (star-x (truncate (+ (* (star-radius s) (cos (star-angle s)) width) (/ width 2))))
           (star-y (truncate (+ (* (star-radius s) (sin (star-angle s)) height) (/ height 2)))))
      (sdl2:set-render-draw-color r value value value alpha)
      (sdl2:render-draw-point r star-x star-y)))
  (sdl2:render-present r))

(defun update-stars (stars max-width max-height dt)
  (dotimes (i (length stars))
    (let ((s (aref stars i)))
      (incf (star-radius s) (* (expt (star-radius s) 1.15) dt (star-speed s)))
      (when (> (* (star-radius s) (max max-width max-height)) (max max-width max-height))
        (setf (star-angle s) (* (random 1.0) 2 pi)
              (star-radius s) (sqrt (random 1.0))
              (star-speed s) (+ 0.002 (random 0.003)))))))

(defun starfield-test ()
  (sdl2:with-init (:video)
    (let ((width 1600)
      (height 900))
      (sdl2:with-window (win :title "Starfield" :w width :h height :flags '(:fullscreen-desktop))
    (sdl2:with-renderer (r win)
      (let ((stars (create-stars 10000))
        (last-time (sdl2:get-ticks)))
        (sdl2:with-event-loop (:method :poll)
          (:quit () t)
          (:idle ()
             (let* ((current-time (sdl2:get-ticks))
                (dt (/ (- current-time last-time) 100.0)))
               (setf last-time current-time)
               (update-stars stars width height dt)
               (render-starfield r stars width height)
               (sdl2:delay 16))))))))))

;; test
;; (defun starfield-test ()
;;   (sdl2:with-init (:video)
;;     (multiple-value-bind (format width height refresh-rate)
;;         (sdl2:get-display-mode 0 0)
;;       (declare (ignore format refresh-rate))
;;       (sdl2:with-window (win :title "Starfield" :w width :h height :flags '(:fullscreen-desktop))
;;       ;; yadda yadda yadda
;;       ))))
